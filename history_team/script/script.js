var chapterActual = 0;
        var chapters = ["Había una vez un joven llamado xxxxx. Era un día soleado en el pueblo donde vivía, pero algo en su corazón le decía que su destino estaba más allá de las colinas que rodeaban su hogar.",
            "Decidió partir en un viaje para explorar el mundo y descubrir su lugar en él. Empacó algunas provisiones y se despidió de su familia y amigos.",
            "Al día siguiente, xxxxx se levantó temprano y continuó su camino. Pasó por valles y montañas, ríos y bosques. Aprendió muchas cosas sobre el mundo y sobre sí mismo en su viaje.", 
        "Finalmente, después de muchas semanas de viajar, xxxxx llegó a una ciudad. Allí encontró un trabajo y comenzó una nueva vida. Pero siempre recordaba su viaje y lo que aprendió en él.", 

        "Y así, xxxxx vivió feliz para siempre, recordando su valiente viaje y sintiendo gratitud por todo lo que aprendió en el camino.</p>"];
        var nombre = '';
        hideOptions();
       
        function addName() {
            nombre = (document.getElementById("input-nombre").value);
            document.getElementById("chapter").innerHTML = chapters[chapterActual].replace("xxxxx", nombre);
            showOptions();
        }

        function back(){
            if(chapterActual >=1){
                chapterActual--;
                document.getElementById("chapter").innerHTML = chapters[chapterActual].replace("xxxxx", nombre);
                showOptions();
            } 
        }

        function next(){
            chapterActual++;
            document.getElementById("chapter").innerHTML = chapters[chapterActual].replace("xxxxx", nombre);
            showOptions();
        }

        function hideOptions() {
            document.getElementById("backButtom").classList.add("hideButtom")
            document.getElementById("nextButtom").classList.add("hideButtom")
        }

        function showOptions() {
            if(chapterActual === 0){
                document.getElementById("backButtom").classList.add("hideButtom")
                document.getElementById("nextButtom").classList.remove("hideButtom")
            } else if(chapterActual > 0 && chapterActual < chapters.length - 1) {
                document.getElementById("backButtom").classList.remove("hideButtom")
                document.getElementById("nextButtom").classList.remove("hideButtom")
            } else if (chapterActual === (chapters.length - 1)){
                console.log("validando");

                document.getElementById("nextButtom").classList.add("hideButtom")
            }
        }